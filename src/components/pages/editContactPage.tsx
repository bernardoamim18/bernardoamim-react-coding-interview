import { IPerson } from "@lib/models/person";
import React, { useState } from "react";
import { useParams } from "react-router-dom";

// Create a submit button
// Get the contact object from array with full IPerson type
// call the update method
// get the result from the call and refresh the state

// interface IEditContactPageProps {
//   onSubmit: (newContactData: IPerson) => void
// }

export const EditContactPage: React.FC = () => {
  const { id } = useParams()

  const [firstName, setFirstName] = useState<string>('')
  const [lastName, setLastName] = useState<string>('')

  const handleSubmit = () => {
    // const newContactData: IPerson = {...oldData, firstName, lastName}

    console.log({firstName, lastName})
    
  }

  return( <>
    <h1>Edit Contact Id: {id}</h1>

    <input type="text" value={firstName} onChange={e => setFirstName(e.target.value) }/>
    <input type="text" value={lastName} onChange={e => setLastName(e.target.value) }/>

    <button type="button" onClick={handleSubmit}>Submit</button>
    </>
  )
}

